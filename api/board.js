let board = [];

export const BoardType = {
    EMPTY: 'empty',
    RANDOM: 'random'
}

const BoardTypeFunctions = {
    [BoardType.EMPTY]: () => false,
    [BoardType.RANDOM]: () => Math.random() < 0.5
}

export const listBoards = () => {
    return []
}

export const resetBoard = (type = BoardType.EMPTY) => {
    return board
}

export const createBoard = (size = 10, type = BoardType.EMPTY) => {
    return board
} 

export const importBoard = (inputBoard) => {
    return board
}

// Suggestion
const countNeighbors = (board, x, y) => {
    var count = 0;
    return count;
}

// Suggestion
const nextGeneration = (board = [[]]) => {
    const next = [];

    return next;
}

export const actual = () => {
    return board
}
export const nextStep = () => {
    return board;
}

export const prevStep = () => {
    return board
}